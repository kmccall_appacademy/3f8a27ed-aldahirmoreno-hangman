require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(opts = {})
    @guesser = opts[:guesser]
    @referee = opts[:referee]
  end

  def setup
    word = referee.pick_secret_word
    guesser.register_secret_length(word)
    @board = (0...word).to_a
  end

  def take_turn
    letter = guesser.guess(board)
    indices = referee.check_guess(letter)
    update_board(indices, letter)
    guesser.handle_response(letter, indices)
  end

  def update_board(indices, letter)
    unless indices.empty?
      indices.each{|x| board[x] = letter}
    end
  end
end

class HumanPlayer
  attr_reader :dictionary

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @dictionary[0].length
  end

  def guess(board)
    letternum = rand(25)
    alpha = ("a".."z").to_a
    alpha[letternum]
  end

  def check_guess(letter)
    @word = @dictionary[0]
    indices = []
    @word.chars.each_with_index{|x,y| indices << y if x == letter}
    indices
  end

  def register_secret_length(num)
    @length = num
    dictionary.reject!{|x| x.length != @length}
  end

  def handle_response(letter, indices)
    if indices.empty?
      dictionary.reject!{|x| x.include?(letter)}
      return "#{letter} is not here"
    else
      dictionary.each{|x|
        x.split("").each_with_index{|y,indx|
          if y == letter
            if indices.include?(indx)
            else
              dictionary.delete(x)
            end
          end
        }
      }
      return "#{letter} is here!"
    end
  end

  def candidate_words
    list = []
    dictionary.each{|x| list << x if x.length == @length}
  end
end

class ComputerPlayer
  attr_reader :dictionary

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @dictionary[0].length
  end

  def guess(board)
    candidate_letter = candidate_words.join("").split("").inject(Hash.new(0)){ |h,v| h[v] += 1; h }
    lttr = candidate_letter.max_by{|k,v| v}
    p candidate_words
    return lttr[0] if candidate_letter.values.count(candidate_letter.values.max) == 1
    letternum = rand(25)
    alpha = ("a".."z").to_a
    alpha[letternum]
  end

  def check_guess(letter)
    @word = @dictionary[0]
    indices = []
    @word.chars.each_with_index{|x,y| indices << y if x == letter}
    indices
  end

  def register_secret_length(num)
    @length = num
    dictionary.reject!{|x| x.length != @length}
  end

  def handle_response(letter, indices)
    if indices.empty?
      dictionary.reject!{|x| x.include?(letter)}
      return "#{letter} is not here"
    else
      dictionary.each{|x|
        x.split("").each_with_index{|y,indx|
          if y == letter
            if indices.include?(indx)
            else
              dictionary.delete(x)
            end
          end
        }
      }
      return "#{letter} is here!"
    end
  end

  def candidate_words
    list = []
    dictionary.each{|x| list << x if x.length == @length}
  end
end
